
QUnit.test( "Basic test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});

QUnit.test("Top News url test", function(assert) {
	assert.ok(getTopNews.url == 'https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty', 'Top news url is valid');
});

QUnit.test("News detail test", function(assert) {
	assert.ok(getNewsItem.url == 'https://hacker-news.firebaseio.com/v0/item/{0}.json?print=pretty', 'News detail url mask is valid');
});
