var getTopNews = function () {
    /*this.fire = function () {
        return $.ajax({ url: this.constructor.url });
    };*/
    $.ajax({
    	url: getTopNews.url,
    	dataType : 'jsonp',
        crossDomain:true,
	    success: function(data) {
	    	var i = 0;
	      	$.each(data, function() {
	      		var child = $('<div><div class="title"></div></div>');
	      		$(child).attr('id', this);
	      		$(child).addClass( i%2 == 0 ? 'evenRow' : 'oddRow');
	      		$(child).append('<div class="newsDetail"><div class="author"></div><div class="nbrComments"></div></div>');
	      		$( "#news" ).append(child);
	      		getNewsItem(this);
	      		i = i + 1;
			});	      
	    }
	});
};
getTopNews.url = 'https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty';

var getNewsItem = function (newsId) {
	var newsdetail = getNewsItem.url.replace('{0}',arguments[0]);
	$.ajax({
    	url: newsdetail,
    	dataType : 'jsonp',
        async:true,
        crossDomain:true,
    	success: function(data) {
    		////console.log(data);
    		$('#'+data['id']).children('.title').append('<a href="'+data['url']+'"" target="_blank">' + data['title'] + '</a>');
    		$('#'+data['id']).attr('comments', data['kids']);
    		$('#'+data['id']).children('.newsDetail').children('.author').text('Author: ' + data['by'] );
    		$('#'+data['id']).children('.newsDetail').children('.nbrComments').text('Comments: ' + $(data['kids']).length );
    	}
    });
}
getNewsItem.url = 'https://hacker-news.firebaseio.com/v0/item/{0}.json?print=pretty';

$(document).ready(function() {
   	getTopNews();
});